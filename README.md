ansible-role-blackbox
=====================

* Main repository (Gitlab) : https://gitlab.com/csanquer-apps/ansible/roles/ansible-role-blackbox

Ansible role to install Stack Overflow [blackbox](https://github.com/StackExchange/blackbox) encryption tool
